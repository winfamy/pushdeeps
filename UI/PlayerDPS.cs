using System;
using Terraria;
using Terraria.UI;
using Terraria.Graphics;
using Terraria.ModLoader;
using Terraria.ModLoader.UI.Elements;
using Terraria.GameContent.UI.Elements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ReLogic.Graphics;
using PushDeeps.UI;

namespace PushDeeps.UI 
{
    class PlayerDPS : UIElement 
    {
        internal int dps;
        internal int maxDps;
        internal int rank;
        internal string name;
        internal int player { get; set; }
        internal Color barColor;
        internal uint LastUpdate { get; set; }
        internal int lastWidth { get; set; }
        internal int TargetWidth;
        internal const int animationTime = 24;

        public PlayerDPS(int r, string n, int d, int m, Color c)
        {
            PaddingTop = 12f;

            rank = r;
            name = n;
            dps = d;
            maxDps = m;
            barColor = c;

            DynamicSpriteFont homespun = PushDeeps.instance.GetFont("Fonts/Homespun");
			Vector2 textSize = new Vector2(homespun.MeasureString(name.ToString()).X, 16f) * 1f;
            this.MinWidth.Set(textSize.X + this.PaddingLeft + this.PaddingRight, 0f);
			this.MinHeight.Set(textSize.Y + this.PaddingTop + this.PaddingBottom, 0f);
        }

        protected override void DrawSelf(SpriteBatch spriteBatch) 
        {
            // set container and bar width
            // maxDps = 80; // testing
            float widthRatio = dps / (float)maxDps;
            Rectangle MeterBarContainer = GetOuterDimensions().ToRectangle();
            Rectangle MeterBar = GetOuterDimensions().ToRectangle();
            TargetWidth = (int)Math.Floor(MeterBarContainer.Width * widthRatio);


            // handle animation
            BezierCurve curve = new BezierCurve(.2, .5, .5, .9);
            int LastWidth = PushDeepsUI.instance.PlayerBarLastWidths[player];

            double ticksPassed = ((double)Main.GameUpdateCount - LastUpdate);
            double ticksRatio = ticksPassed / animationTime;
            ticksRatio = ticksRatio > 1 ? 1 : ticksRatio;
            MeterBar.Width = (int)(LastWidth + (TargetWidth - LastWidth) * curve.solve(ticksRatio, .01));
            if (MeterBar.Width < TargetWidth) {
                PushDeepsUI.instance.updateNeeded = true;
            }
            else if (MeterBar.Width == TargetWidth)
            {
                PushDeepsUI.instance.PlayerBarLastWidths[player] = MeterBar.Width;
            }


            // draw texture
            Texture2D bantoTexture = PushDeeps.instance.GetTexture("Media/BantoBar");
            Rectangle srcRectangle = new Rectangle(0, 0, (int)(bantoTexture.Width * widthRatio), bantoTexture.Height);
            Main.spriteBatch.Draw(bantoTexture, MeterBar, srcRectangle, barColor);


            // setup font stuff
            Color textColor = Color.White;
            DynamicSpriteFont homespun = PushDeeps.instance.GetFont("Fonts/Homespun");
			Vector2 vector = homespun.MeasureString(name);


            // draw left text
            Terraria.UI.Chat.ChatManager.DrawColorCodedStringWithShadow(spriteBatch, homespun, rank.ToString(), MeterBarContainer.TopLeft() + new Vector2(8, 8), textColor, 0f,
				new Vector2(0, 0) * vector, new Vector2(.8f), -1f, 1f);
			Terraria.UI.Chat.ChatManager.DrawColorCodedStringWithShadow(spriteBatch, homespun, name, MeterBarContainer.TopLeft() + new Vector2(26, 5), textColor, 0f,
				new Vector2(0, 0) * vector, new Vector2(1f), -1f, 1f);


            // draw right text
			string rightText = dps.ToString();
			vector = homespun.MeasureString(rightText);
			Terraria.UI.Chat.ChatManager.DrawColorCodedStringWithShadow(spriteBatch, homespun, rightText, MeterBarContainer.TopRight() + new Vector2(-8, 5), textColor, 0f,
				new Vector2(1f, 0) * vector, new Vector2(1f), -1f, 1f);
        }
    }

    internal class BezierCurve 
    {
        internal double ax, ay;
        internal double bx, by;
        internal double cx, cy;

        public BezierCurve(double p1x, double p1y, double p2x, double p2y)
        {
            cx = 3.0 * p1x;
            bx = 3.0 * (p2x - p1x) - cx;
            ax = 1.0 - cx -bx;

            cy = 3.0 * p1y;
            by = 3.0 * (p2y - p1y) - cy;
            ay = 1.0 - cy - by;
        }

        internal double sampleCurveX(double t)
        {
            return ((ax * t + bx) * t + cx) * t;
        }

        internal double sampleCurveY(double t)
        {
            return ((ay * t + by) * t + cy) * t;
        }

        internal double sampleCurveDerivativeX(double t)
        {
            return (3.0 * ax * t + 2.0 * bx) * t + cx;
        }

        internal double solveCurveX(double x, double epsilon)
        {
            double t0;
            double t1;
            double t2;
            double x2;
            double d2;
            int i;

            // newton's method
            for (t2 = x, i = 0; i < 8; i++) {
                x2 = sampleCurveX(t2) - x;
                if (Math.Abs (x2) < epsilon)
                    return t2;
                d2 = sampleCurveDerivativeX(t2);
                if (Math.Abs(d2) < 1e-6)
                    break;
                t2 = t2 - x2 / d2;
            }

            // bisection method
            t0 = 0.0;
            t1 = 1.0;
            t2 = x;

            if (t2 < t0)
                return t0;
            if (t2 > t1)
                return t1;

            while (t0 < t1) {
                x2 = sampleCurveX(t2);
                if (Math.Abs(x2 - x) < epsilon)
                    return t2;
                if (x > x2)
                    t0 = t2;
                else
                    t1 = t2;
                t2 = (t1 - t0) * .5 + t0;
            }

            // Failure.
            return t2;
        }

        internal double solve(double x, double epsilon)
        {
            return sampleCurveY(solveCurveX(x, epsilon));
        }
    }
}