using Terraria.UI;
using Terraria;
using Terraria.Graphics;
using Terraria.ModLoader.UI.Elements;
using Terraria.GameContent.UI.Elements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PushDeeps.UI 
{
    internal class DraggableUIPanel : UIPanel
    {
        private Vector2 offset;
        public bool dragging = false;

        public DraggableUIPanel()
        {
            this.OnMouseDown += new UIElement.MouseEvent(DragStart);
            this.OnMouseUp += new UIElement.MouseEvent(DragEnd);
        }

        private void DragStart(UIMouseEvent evt, UIElement listeningElement)
        {
            offset = new Vector2(evt.MousePosition.X - Left.Pixels, evt.MousePosition.Y - Top.Pixels);
            dragging = true;
        }

        private void DragEnd(UIMouseEvent evt, UIElement listeningElement)
        {
            Vector2 end = evt.MousePosition;
            dragging = false;

            Left.Set(end.X - offset.X, 0f);
            Top.Set(end.Y - offset.Y, 0f);

            Recalculate();
        }

        protected override void DrawSelf(SpriteBatch spriteBatch) 
        {
			if (dragging)
			{
				Left.Set(Main.MouseScreen.X - offset.X, 0f);
				Top.Set(Main.MouseScreen.Y - offset.Y, 0f);
				Recalculate();
			}

            // base.DrawSelf(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
			if (dragging)
			{
				Left.Set(Main.MouseScreen.X - offset.X, 0f);
				Top.Set(Main.MouseScreen.Y - offset.Y, 0f);
				Recalculate();
			}
        }
    }
}