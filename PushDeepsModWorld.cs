using Terraria;
using Terraria.ModLoader;

namespace PushDeeps 
{
    internal class PushDeepsModWorld : ModWorld 
    {
        public override void PostUpdate() 
        {
            if (PushDeeps.instance.CurrentFight != null && !PushDeeps.instance.CurrentFight.IsDone()) {
                if (Main.GameUpdateCount % 60 == 0)
                {
                    // sync
                    PushDeeps.instance.CurrentFight.BroadcastSelf(PushDeepsMessageType.ReportIntervalDPS);
                }
            }
        }
    }
}