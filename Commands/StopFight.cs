using Terraria;
using Terraria.ModLoader;
using Terraria.Localization;
using Microsoft.Xna.Framework;

namespace PushDeeps
{
    internal class StopFight : ModCommand
    {
        public override CommandType Type => CommandType.World;
        public override string Command => "stopfight";
        public override string Description => "Force ends the world's current fight";

        public override void Action (CommandCaller caller, string input, string[] args) 
        {
            if (PushDeeps.instance.CurrentFight != null) {
                NetMessage.BroadcastChatMessage(NetworkText.FromLiteral("Ended current fight"), Color.Orange, -1);
                PushDeeps.instance.CurrentFight.EndFight();
            }
        }
    }
}