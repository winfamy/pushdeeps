using Terraria;
using Terraria.ModLoader;

namespace PushDeeps.Commands 
{
    internal class DebugDPS : ModCommand 
    {
        public override CommandType Type => CommandType.Console;
        public override string Command => "pushdeeps";
        public override string Description => "I suck at development";

        public override void Action (CommandCaller caller, string input, string[] args) 
        {
            if (PushDeeps.instance != null) {
                System.Console.WriteLine("instance ok");
                if (PushDeeps.instance.bossIds != null) {
                    System.Console.WriteLine("bossids ok");
                    
                    foreach (var id in PushDeeps.instance.bossIds)
                    {
                        System.Console.WriteLine(id);
                    }
                }
                
            }
        }
    }
}