using System;
using System.Collections.Generic;
using Terraria;
using Terraria.UI;
using Terraria.Graphics;
using Terraria.ModLoader;
using Terraria.ModLoader.UI.Elements;
using Terraria.GameContent.UI.Elements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using PushDeeps.UI;

namespace PushDeeps
{
    internal class PushDeepsUI : UIState
    {
        internal static PushDeepsUI instance;
        internal DraggableUIPanel MainPanel;
        internal UIText PanelHeaderText;
        internal UIText WaitingText;
        internal UIGrid DPSMeter;
        internal UIText StatusText;
        internal Color[] ColorList;
        internal int[] PlayerBarLastWidths;
        internal int DefaultMainHeight;

        public bool Visible {
			get { return PushDeeps.instance.DeepsInterface.CurrentState == PushDeeps.instance.DeepsUI; }
			set { PushDeeps.instance.DeepsInterface.SetState(value ?  PushDeeps.instance.DeepsUI : null); }
		}

        public PushDeepsUI()
        {
            instance = this;

            lastFightUpdate = false;
            updateNeeded = false;
            PlayerBarLastWidths = new int[256];
            Array.Clear(PlayerBarLastWidths, 0, PlayerBarLastWidths.Length);
        }

        public override void OnInitialize()
        {
            Random randomColor = new Random();
            ColorList = new Color[16];
            for (int i = 0; i < 16; i++)
            {
                ColorList[i] = new Color( randomColor.Next(255), randomColor.Next(255), randomColor.Next(255) );
            }

            MainPanel = new DraggableUIPanel();
            MainPanel.Width.Set(300, 0);
            DefaultMainHeight = 150;
            MainPanel.Height.Set(DefaultMainHeight, 0);
            MainPanel.MarginRight = 100;
            MainPanel.MarginBottom = 100;
            MainPanel.Top.Set(Main.screenHeight - MainPanel.Height.GetValue(1f) - MainPanel.MarginBottom, 0);
            MainPanel.Left.Set(Main.screenWidth - MainPanel.Width.GetValue(1f) - MainPanel.MarginRight, 0);
            MainPanel.BackgroundColor = new Color(0,0,0) * 0f;
            // MainPanel.Set.Top(); 
            Append(MainPanel);

            PanelHeaderText = new UIText("PushDeeps", .6f);
            MainPanel.Append(PanelHeaderText);

            WaitingText = new UIText("Hit a boss or event\nmob to start fight.");
            WaitingText.VAlign = 0.5f;
            WaitingText.HAlign = 0.5f;
            // DefaultText.textScale = .6f;
            MainPanel.Append(WaitingText);

            var labelDimensions = PanelHeaderText.GetInnerDimensions();
			int top = (int)labelDimensions.Height + 15;

            DPSMeter = new UIGrid();
            DPSMeter.Width.Set(0, 1f);
			DPSMeter.Height.Set(-top, 1f);
            DPSMeter.Top.Set(top, 0f);
			DPSMeter.ListPadding = 0f;
            MainPanel.Append(DPSMeter);

            StatusText = new UIText("Waiting", .6f);
            StatusText.VAlign = 0f;
            StatusText.HAlign = 1f;
            MainPanel.Append(StatusText);
        }

		internal uint LastUpdate;
        internal bool updateNeeded;
        internal bool lastFightUpdate;
		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
            if (updateNeeded) {
                UpdateMeter();
                updateNeeded = false;
            }
		}

        internal void UpdateMeter() 
        {
            if (PushDeeps.instance.CurrentFight == null) 
            {
                return;
            }

            WaitingText.SetText("");
            StatusText.SetText("Running");
            if (lastFightUpdate)
            {
                StatusText.SetText("Finished");
                StatusText.Recalculate();
                Array.Clear(PlayerBarLastWidths, 0, PlayerBarLastWidths.Length);
                return;
            }

            DPSMeter.Clear();
            if (Main.netMode == 1) 
            {
                int[] damage = PushDeeps.instance.CurrentFight.PlayerDamage;
                List<Tuple<int, int>> damageList = new List<Tuple<int, int>>();
                for (int i = 0; i < 256; i++)
                {
                    if (damage[i] > 0 && i != 255)
                    {
                        damageList.Add(new Tuple<int, int>(i, damage[i]));
                    }
                }

                damageList.Sort((a, b) => b.Item2.CompareTo(a.Item2));
                if (damageList.Count > 0) {
                    int maxDps = (int)(damageList[0].Item2 / ((float)(PushDeeps.instance.CurrentFight.ServerTick - PushDeeps.instance.CurrentFight.StartTick) / 60));
                    for (int i = 0; i < damageList.Count; i++) 
                    {
                        if (damageList[i].Item2 > 0) 
                        {
                            int dps = (int)(damageList[i].Item2 / ((float)(PushDeeps.instance.CurrentFight.ServerTick - PushDeeps.instance.CurrentFight.StartTick) / 60));
                            PlayerDPS test = new PlayerDPS(i + 1, Main.player[damageList[i].Item1].name, dps, maxDps, ColorList[ damageList[i].Item1 % ColorList.Length]);

                            test.LastUpdate = LastUpdate;
                            test.player = damageList[i].Item1;

                            test.Width.Set(0, 1f);
                            DPSMeter.Add(test);
                        }
                    }
                }

                DPSMeter.Recalculate();
                if (DPSMeter.Height.Pixels + PanelHeaderText.GetInnerDimensions().Height >= DefaultMainHeight) 
                {
                    MainPanel.Height.Pixels = DPSMeter.Height.Pixels + PanelHeaderText.GetInnerDimensions().Height;
                    MainPanel.Recalculate();
                } else {
                    MainPanel.Height.Pixels = DefaultMainHeight;
                    MainPanel.Recalculate();
                }
            }
            // test.Width.Set(200, 0);
            // DPSMeter.Add(test);
        }
    }
}