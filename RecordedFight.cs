using Terraria;
using Terraria.ModLoader;
using Terraria.Localization;
using System.Collections.Generic;
using System.Text;
using System;
using Microsoft.Xna.Framework;

namespace PushDeeps 
{
	internal class RecordedFight 
	{
		internal uint StartTick { get; set; }
		internal uint EndTick { get; set; } 
		internal uint ServerTick { get; set; }
		internal int FightDepth { get; set; }
		internal int[] PlayerDamage;
		internal List<int> RecordedNpcIDs;
		internal bool RecordingInvasion;
		internal bool BroadcastedEnd;
		internal bool FightDone;

		public RecordedFight () 
        {
			PlayerDamage = new int[256];
            RecordedNpcIDs = new List<int>();
			StartTick = Main.GameUpdateCount;
			for (int i = 0; i < 256; i++) {
				PlayerDamage[i] = -1;
			}

			BroadcastStart();
		}

        public bool IsDone() 
        {  
            return FightDone;
        }

		public void EndFight() 
        {
			EndTick = Main.GameUpdateCount;
			FightDone = true;
            foreach (int npcIndex in RecordedNpcIDs) {
                NPC npc = Main.npc[npcIndex];
                PushDeepsGlobalNPC modNPC = npc.GetGlobalNPC<PushDeepsGlobalNPC>();
                modNPC.HasCausedRecord = false;
            }

			if (!BroadcastedEnd)
			{
    	        BroadcastEnd();
				BroadcastedEnd = true;
			}
		}

		public void AddNpc(int npcIndex) 
        {
			RecordedNpcIDs.Add(npcIndex);
		}

		public void NpcDeath(int npcIndex) 
        {
			if (RecordedNpcIDs.Contains(npcIndex)) 
			{
				if (--FightDepth <= 0) 
				{

					if (RecordingInvasion && Main.invasionType == 0) 
					{
						EndFight();
					} 
					else if (!RecordingInvasion) // not recording invasion, end
					{
						EndFight();
					}
				}
			}
		}

		public void AddPlayerDamage(int player, int damage) 
        {
			PlayerDamage[player] += damage;
		}

		public void BroadcastStart() 
        {
			BroadcastSelf(PushDeepsMessageType.StartBossDPSRecords);
		}

        public void BroadcastEnd() 
        {
			BroadcastSelf(PushDeepsMessageType.EndBossDPSRecords);
			BroadcastSelf(PushDeepsMessageType.ReportFightTotal);

			StringBuilder sb = new StringBuilder();
			sb.Append("Fight finished in ");
			sb.Append(String.Format("{0:0.00}", ((double) EndTick - StartTick) / 60));
			sb.Append(" seconds");
			NetMessage.BroadcastChatMessage(NetworkText.FromLiteral(sb.ToString()), Color.Orange);

			for (int i = 0; i < 255; i++)
			{
				sb.Clear();
				if (PlayerDamage[i] > 0)
				{
					sb.Append(Main.player[i].name);
					sb.Append(" - ");
					sb.Append(PlayerDamage[i]);
					NetMessage.BroadcastChatMessage(NetworkText.FromLiteral(sb.ToString()), Color.Orange);
				}
			}
		}

		public void BroadcastSelf(PushDeepsMessageType type) 
        {
            if (Main.netMode != 2)
                return;

		    var message = PushDeeps.instance.GetPacket();
			message.Write((byte)type);

			// fight meta
			message.Write(StartTick);
            if (type == PushDeepsMessageType.EndBossDPSRecords || type == PushDeepsMessageType.ReportFightTotal)
            {
                message.Write(EndTick);
            }

            // just send type & tick counts
            if (type == PushDeepsMessageType.EndBossDPSRecords || type == PushDeepsMessageType.StartBossDPSRecords) 
            {
                message.Send();
                return;
            }

			message.Write(Main.GameUpdateCount);

			// player count
			byte count = 0;
			for (int i = 0; i < 256; i++)
			{
				if (Main.player[i].active && PlayerDamage[i] > 0) {
					count++;
				}
			}
			message.Write(count);
			
			// player damage
			for (int i = 0; i < 256; i++)
			{
				if (Main.player[i].active && PlayerDamage[i] > 0) 
				{
					message.Write((byte)i);
					message.Write(PlayerDamage[i]);
				}
			}

			message.Send();
		}
	}
}