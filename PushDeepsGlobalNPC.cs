using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Text;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using System;
using Terraria.Localization;

namespace PushDeeps 
{
    internal class PushDeepsGlobalNPC : GlobalNPC
    {
        public override bool InstancePerEntity => true;

        internal bool HasDied = false;
        internal bool HasCausedRecord = false;
        internal Color MessageColor = Color.Orange;

        public override void NPCLoot(NPC npc) 
        {
            if (PushDeeps.instance.CurrentFight != null) {
                PushDeeps.instance.CurrentFight.NpcDeath(npc.whoAmI);
            }
        }
    }
}