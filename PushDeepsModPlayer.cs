using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.GameInput;

namespace PushDeeps
{
    internal class PushDeepsModPlayer : ModPlayer
    {

        public override void ProcessTriggers(TriggersSet triggersSet)
        {
            if (Main.netMode == 1) 
            {
                if (PushDeeps.ToggleDPSKey.JustPressed)
                {
                    PushDeeps.instance.DeepsUI.updateNeeded = true;
                    PushDeeps.instance.DeepsUI.Visible = !PushDeeps.instance.DeepsUI.Visible;
                }
            }
        }
    }
}