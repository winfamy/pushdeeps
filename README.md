This is a really cool DPS Meter for Terraria using the tModLoader API along with a custom-build of BossChecklist.  
Animation is done using cubic bezier curves defined in [UI/PlayerDPS.cs](https://gitlab.com/winfamy/pushdeeps/-/blob/master/UI/PlayerDPS.cs)

Not to be published on the tModLoader Mod Browser.

Demo: https://www.youtube.com/watch?v=Vt-8p21Ej_M
