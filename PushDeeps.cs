using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using System.IO;
using System;
using Terraria.UI;
using Terraria.Graphics;
using BossChecklist;
using Microsoft.Xna.Framework;
using System.Text;

namespace PushDeeps
{
	public class PushDeeps : Mod
	{	
		internal static PushDeeps instance;
		internal const int UPDATE_RATE = 20;

		internal static ModHotKey ToggleDPSKey;
		// internal ModHotKey ToggleBreakdownKey;

		internal List<int> bossIds = new List<int>();
		internal List<int> invasionIds = new List<int>();
		internal RecordedFight CurrentFight;

		internal UserInterface DeepsInterface;
		internal PushDeepsUI DeepsUI;

		public PushDeeps() {}

		public override void Load() 
		{
			instance = this;

			ToggleDPSKey = RegisterHotKey("Toggle DPS Chart", "F6");
			// ToggleBreakdownKey 	= RegisterHotKey("Toggle Damage Breakdown", "F7");

			if (!Main.dedServ)
			{
				DeepsInterface = new UserInterface();
				DeepsUI = new PushDeepsUI();
				DeepsUI.Activate();

				DeepsInterface.SetState(DeepsUI);
			}
		}

		public override void Unload() {
			ToggleDPSKey = null;
			// ToggleBreakdownKey = null;
			instance = null;
		}

		public override void PostAddRecipes() {
			Mod bossChecklist = ModLoader.GetMod("BossChecklist");
			if (bossChecklist != null) {
				try {
					var bossDict = bossChecklist.Call("GetBossInfoDictionary");
					if (bossDict.GetType() != "".GetType()) 
					{
						Logger.Debug("bosschecklist called");
						foreach (var boss in ((Dictionary<string, Dictionary<string, object>>)bossDict).Values)
						{
							object tempNpcIds;
							object isBoss, isMiniBoss;
							boss.TryGetValue("npcIDs", out tempNpcIds);
							boss.TryGetValue("isBoss", out isBoss);
							boss.TryGetValue("isMiniboss", out isMiniBoss);
							
							if (tempNpcIds != null) {
								foreach (int npcId in (List<int>)tempNpcIds)
								{
									if ((bool)isBoss || (bool)isMiniBoss)
									{
										bossIds.Add(npcId);
									}
									else 
									{
										System.Console.WriteLine("found INVASION npc");
										invasionIds.Add(npcId);
									}
								}
							} else {
								Logger.Debug("tempBossIds null");
							}
						}
					}
				} 
				catch (Exception e)
				{
					Logger.Debug(e.Message);
				}
			}
		}

        public override bool HijackGetData(ref byte messageType, ref BinaryReader reader, int playerNumber)
        {
            try
            {
				if (messageType == MessageID.StrikeNPC && Main.netMode == NetmodeID.Server) {
					int npcIndex = reader.ReadInt16();
					int damage = reader.ReadInt16();
					
					NPC npc = Main.npc[npcIndex];
					if (npc.realLife >= 0) 
					{
						npc = Main.npc[npc.realLife];
						npcIndex = npc.realLife;
					}
				
					PushDeepsGlobalNPC modNPC = npc.GetGlobalNPC<PushDeepsGlobalNPC>();
					double defenseCoefficient = Main.expertMode ? .75 : .5;
					int netDamage = (int)Math.Ceiling(damage - npc.defense * defenseCoefficient);
					netDamage = netDamage > 0 ? netDamage : 1;

					if (bossIds.Contains(npc.type)) 
					{	
						if (CurrentFight != null && !CurrentFight.IsDone()) 
						{
							if (!CurrentFight.RecordedNpcIDs.Contains(npcIndex)) 
							{
								CurrentFight.AddNpc(npcIndex);
								CurrentFight.FightDepth++;
							}
						} 
						else
						{
							CurrentFight = new RecordedFight();
							CurrentFight.AddNpc(npcIndex);
							CurrentFight.FightDepth++;
						}

						CurrentFight.AddPlayerDamage(playerNumber, netDamage);
					}
					else if (invasionIds.Contains(npc.type))
					{
						if (CurrentFight != null && !CurrentFight.IsDone()) // currentfight not over, continue recording
						{
							CurrentFight.RecordingInvasion = true;
							if (!CurrentFight.RecordedNpcIDs.Contains(npcIndex)) 
							{
								CurrentFight.AddNpc(npcIndex);
								CurrentFight.FightDepth++;
							}

							CurrentFight.AddPlayerDamage(playerNumber, netDamage);
						}

						// start recording if invasionType not 0
						if ((CurrentFight == null || CurrentFight.IsDone()) && Main.invasionType != 0)
						{
							CurrentFight = new RecordedFight();
							CurrentFight.RecordingInvasion = true;
							CurrentFight.AddNpc(npcIndex);
							CurrentFight.FightDepth++;
							CurrentFight.AddPlayerDamage(playerNumber, netDamage);
						}
					}
				}
            }
            catch (Exception)
            {
                // 
            }

			return false;
        }

		public override void HandlePacket(BinaryReader reader, int whoAmI) 
		{
			PushDeepsMessageType type = (PushDeepsMessageType)reader.ReadByte();


			switch(type) 
			{
				case PushDeepsMessageType.StartBossDPSRecords:
					CurrentFight = new RecordedFight();
					CurrentFight.StartTick = (uint)reader.ReadUInt32();

					if (!Main.dedServ) {
						PushDeepsUI.instance.lastFightUpdate = false;				
					}

					break;
				case PushDeepsMessageType.EndBossDPSRecords:
					CurrentFight = new RecordedFight();
					CurrentFight.StartTick = (uint)reader.ReadUInt32();
					CurrentFight.EndTick = (uint)reader.ReadUInt32();
					// Main.NewText(CurrentFight.StartTick.ToString());
					// Main.NewText(CurrentFight.EndTick.ToString());
					// Main.NewText(CurrentFight.StartTick.ToString());
					break;
				case PushDeepsMessageType.ReportIntervalDPS:
					CurrentFight = new RecordedFight();
					CurrentFight.StartTick = (uint)reader.ReadUInt32();
					CurrentFight.ServerTick = (uint)reader.ReadUInt32();
					byte count = (byte)reader.ReadByte();
					for (int i = 0; i < count; i++)
					{
						byte playerId = (byte)reader.ReadByte();
						int damage = (int)reader.ReadInt32();
						// Main.NewText($"Player {Main.player[playerId].name} Damage {(int)damage}");
						CurrentFight.AddPlayerDamage(playerId, damage);
					}

					if (!Main.dedServ) {
						PushDeepsUI.instance.updateNeeded = true;
						PushDeepsUI.instance.LastUpdate = Main.GameUpdateCount;						
					}

					break;
				case PushDeepsMessageType.ReportFightTotal:
					CurrentFight.StartTick = (uint)reader.ReadUInt32();
					CurrentFight.EndTick = (uint)reader.ReadUInt32();
					CurrentFight.ServerTick = (uint)reader.ReadUInt32();
					count = (byte)reader.ReadByte();
					for (int i = 0; i < count; i++)
					{
						byte playerId = (byte)reader.ReadByte();
						int damage = (int)reader.ReadInt32();
						// Main.NewText($"Player {Main.player[playerId].name} Damage {(int)damage}");
						CurrentFight.AddPlayerDamage(playerId, damage);
					}

					if (!Main.dedServ) {
						PushDeepsUI.instance.updateNeeded = true;
						PushDeepsUI.instance.lastFightUpdate = true;				
					}

					break;
			}
		}

		private GameTime _lastUpdateUiGameTime;
		public override void UpdateUI(GameTime gameTime) {
			_lastUpdateUiGameTime = gameTime;
			if (DeepsInterface?.CurrentState != null) {
				DeepsInterface.Update(gameTime);
			}
		}

		public override void ModifyInterfaceLayers(List<GameInterfaceLayer> layers) {
			int mouseTextIndex = layers.FindIndex(layer => layer.Name.Equals("Vanilla: Mouse Text"));
			if (mouseTextIndex != -1) {
				layers.Insert(mouseTextIndex, new LegacyGameInterfaceLayer(
					"PushDeeps: DPS Meter",
					delegate
					{
						if ( _lastUpdateUiGameTime != null && DeepsInterface?.CurrentState != null) {
							DeepsInterface.Draw(Main.spriteBatch, _lastUpdateUiGameTime);
						}
						return true;
					},
					InterfaceScaleType.UI));
			}
		}

		internal void ShowUI()
		{
			DeepsInterface?.SetState(DeepsUI);
		}

		internal void HideUI() 
		{
			DeepsInterface?.SetState(null);
		}
	}


	enum PushDeepsMessageType : byte 
	{
		// StartBossDPSRecords
		// byte type
		// int 	startTick
		StartBossDPSRecords,

		// EndBossDPSRecords
		// byte 	type
		// int 		startTick
		// int		endTick
		EndBossDPSRecords,

		// ReportIntervalDPS
		// byte 	type
		// int		startTick
		// byte		count
		// int[] 	playerDamage
		ReportIntervalDPS,

		// ReportFightTotal
		// byte		type
		// int		startTick
		// int		endTick
		// byte		count
		// int[]	playerDamage
		ReportFightTotal
	}
}